namespace OtusDelegates.DATA
{
    public class DataItemDouble
    {
        public double DoubleValue { get; set; }
        
        public override string ToString()
        {
            return $"DoubleValue={DoubleValue}";
        }
    }
}
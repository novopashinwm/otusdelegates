namespace OtusDelegates.DATA
{
    public class DataItemInt
    {
        public int IntValue { get; set; }

        public override string ToString()
        {
            return $"IntValue={IntValue}";
        }
    }
}
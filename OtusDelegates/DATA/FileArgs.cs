using System;

namespace OtusDelegates.DATA
{
    public class FileArgs: EventArgs
    {
        public void Show(string file)
        {
            Console.WriteLine($"Нашелся файл {file}");
        }
    }
}
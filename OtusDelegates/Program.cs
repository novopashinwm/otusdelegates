﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using OtusDelegates.DATA;

namespace OtusDelegates
{
    public class Program
    {
        static Random _random = new Random();
        
        public static void Main(string[] args)
        {
            #region максимальное значение коллекции int

            int min = 10, max = 100;
            List<DataItemInt> rowsI = new List<DataItemInt>()
            {
                new DataItemInt(){ IntValue = _random.Next(min,max)},
                new DataItemInt() {IntValue = _random.Next(min,max)},
                new DataItemInt() {IntValue = _random.Next(min,max)},
                new DataItemInt() {IntValue = _random.Next(min,max)},
                new DataItemInt() {IntValue = _random.Next(min,max)},
            };
            var maxInt = rowsI.GetMax((a, b) => a.IntValue > b.IntValue ? a : b);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write($"Максимальное значение в коллекции {typeof(DataItemInt).Name}: ");
            Console.ResetColor();
            Console.WriteLine(maxInt);
            #endregion

            Console.WriteLine();
            
            #region максимальное значение коллекции float
            List<DataItemDouble> rowsF = new List<DataItemDouble>()
            {
                new DataItemDouble(){ DoubleValue =  _random.NextDouble()* 100.0 - 10.0},
                new DataItemDouble() {DoubleValue =  _random.NextDouble()* 100.0 - 10.0},
                new DataItemDouble() {DoubleValue =  _random.NextDouble()* 100.0 - 10.0},
                new DataItemDouble() {DoubleValue =  _random.NextDouble()* 100.0 - 10.0},
                new DataItemDouble() {DoubleValue =  _random.NextDouble()* 100.0 - 10.0},
            };
            var maxDouble = rowsF.GetMax((a, b) => a.DoubleValue > b.DoubleValue ? a : b);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write($"Максимальное значение в коллекции {typeof(DataItemDouble).Name}: ");
            Console.ResetColor();
            Console.WriteLine(maxDouble);
            #endregion

            Console.WriteLine();
            Console.WriteLine("Поиск файлов:");
            List<string> files = new List<string>();
            for (int i = 0; i < 50; i++)
            {
                files.Add($"file{_random.Next(1000,9999)}.txt");
            }
        
            FileFinder finder = new FileFinder();
            finder.FindFilesDirectory(files);

        }
    }
    
    public static class Ext
    {
        public static T GetMax<T>(this IEnumerable<T> collection, Func<T, T, T> getParametr) where T : class
        {
            return collection.Aggregate(getParametr);
        }
    }

}
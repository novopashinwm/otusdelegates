using System;
using System.Collections.Generic;
using OtusDelegates.DATA;

namespace OtusDelegates
{
    public class FileFinder
    {
        public event EventHandler<FileArgs> FileFound;

        public void FindFilesDirectory(List<string> files)
        {
            FileArgs mess = new FileArgs();
            foreach (var file in files)
            {
                mess.Show(file);
                OnFound(mess);
                CheckEnd();
            }
        }

        private void CheckEnd()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Нажмите любую клавишу для продолжения и  0 - для отмены поиска");
            Console.ResetColor();
            if (Console.ReadKey().Key == ConsoleKey.D0)
                FileFound += OnEnd;
        }

        private void OnEnd(object sender, FileArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Окончание обработки");
            Console.WriteLine();
            Environment.Exit(0);
        }

        private void OnFound(FileArgs e)
        {
            FileFound?.Invoke(this,e);
        }
    }
}